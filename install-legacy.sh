# !/bin/bash

# Load US Keymap
loadkeys us.map.gz

# Set timedate to accurate values
timedatectl set-ntp true

while true; do

#Partitioning the Disk
lsblk

disk='/dev/sda'

echo "Enter the drive address you want to install arch on: "  
read disk  

cfdisk $disk

lsblk

echo "Is the drive partition ok? (y/Y) "  
    read -N 1 input
    if [[ $input = "y" ]] || [[ $input = "Y" ]]; then
        break 
    fi

done

# Partition the drive

echo "Enter the partition for the arch system: "  
read main_partition  

mkfs.ext4 $main_partition

echo "Did you made a swap partition? (y/Y) "  
    read -N 1 input
    if [[ $input = "y" ]] || [[ $input = "Y" ]]; then
        echo "Enter the partition for the swap: "  
        read swap_partition  
        mkswap $swap_partition
        swapon $swap_partition
    fi

# Mounting the Partition
mount $main_partition /mnt 


# Installing Reflector
pacstrap /mnt reflector

# Optimizing mirrors
reflector -c "India" -p http --sort rate >> /etc/pacman.d/mirrorlist

# Installing Essential Packages
pacstrap /mnt base base-devel linux linux-firmware linux-headers nano


# Generate fstab
genfstab -U /mnt >> /mnt/etc/fstab


#chroot into arch
arch-chroot /mnt

# Set the timezone
ln -sf /usr/share/zoneinfo/Asia/Kolkata /etc/localtime

# Set the hardware clock to GMT
hwclock --systohc

# Setting Localization
echo "Enter the language code for the arch system (Example: en_US): "  
read language  

echo "$language.UTF-8 UTF-8" >> /etc/locale.gen
locale-gen
echo "LANG=$language.UTF-8" > /etc/locale.conf
export LANG=$language.UTF-8

# Setting Up Hostname
echo "Enter the hostname for the arch system (Example: arch): "  
read hostname
echo $hostname > /etc/hostname

# Setting up hosts file
echo '127.0.0.1 localhost' >> /etc/hosts
echo '::1		localhost' >> /etc/hosts
echo "127.0.1.1 $hostname.localdomain $hostname" >> /etc/hosts
cat /etc/hosts

#creating a new user
echo "Enter the username for the arch system admin (Example: dinodroid): "  
read username
useradd -m -G wheel -s /bin/bash $username
passwd $username


# Uncomment the line that states "Uncomment to allow members of group wheel to execute any command
echo "Uncomment the line that states Uncomment to allow members of group wheel to execute any command"
EDITIOR=nano visudo

# Uncomment the two lines that say [multilib]
echo "Uncomment the two lines that say [multilib]"
nano /etc/pacman.conf 

# Sync the repos
pacman -Syy 


#Install grub packages
pacman -S grub os-prober

#Install grub 
grub-install --target=i386-pc $disk
grub-mkconfig -o /boot/grub/grub.cfg


# Installing Visual packages
pacman -S xorg xorg-apps xorg-server xorg-drivers mesa 


# Installing network manager packages
pacman -S networkmanager network-manager-applet

# Installing LightDM 
pacman -S lightdm lightdm-gtk-greeter

# Installing a Desktop environment
pacman -S xfce4 xfce4-goodies

# Enabling Services
systemctl enable lightdm.service
systemctl enable NetworkManager.service

# Exiting chroot
exit

# Umount all
umount -a

# Reboot the computer
reboot










